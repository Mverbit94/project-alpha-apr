from django.shortcuts import render, redirect
from .forms import CreateTaskForm
from .models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()

    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "list_tasks": tasks,
    }
    return render(request, "tasks/mine.html", context)
