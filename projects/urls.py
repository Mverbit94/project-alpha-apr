from django.urls import path
from .views import list_projects, project_detail, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_detail, name="project_detail"),
    path("create/", create_project, name="create_project"),
]
