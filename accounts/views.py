from django.shortcuts import render, redirect
from django.contrib.auth import (
    authenticate,
    login as user_login,
    logout as user_logout,
)
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm

# Create your views here.


def login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                user_login(request, user)
                return redirect("home")

    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout(request):
    user_logout(request)
    return redirect("home")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                user_login(request, user)
                return redirect("projects/list_projects")

    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
